$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000;
    });
    //Sintaxis JQuery 
    $('#contacto').on('show.bs.modal', function(e) { //el on es para suscribir eventos
        console.log('El modal se esta mostrando'); //para escribir en consola 
        $('#contactoBtn').removeClass('btn-online-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('hidden.bs.modal', function(e) {
        $('#contactoBtn').prop('disabled', false);
    });
});